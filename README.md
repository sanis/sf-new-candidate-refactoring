### Refactoring'as

Karolis nebuvo labai geras programuotojas. Paliktas be priežiūros jis įgyvendino šią aplikaciją kaip mokėjo
ir paleido ją gyventi. Paviršutiniški testai praėjo, tačiau jos niekas daugiau nejudino ir į live nepaleido.
Vieną dieną, Karolis išėjo parūkyti ir niekada nebegrįžo į darbą. Taip jau būna dirbant Vandenilyje ¯\_(ツ)_/¯

Šiandien tu atėjai užimti Karolio vietos. Prieš tave - Karolio aplikacija. Niekas nedrįsom perimti šito
kodo, bet kaip naujokas tu laimėjai šią loteriją.

Tavo tikslas - pataisyti šį kodą taip, kad nebijotum toliau jo plėsti, būtu saugu paleisti į live aplinką
ir kolegų nemuštu šaltas prakaitas užmetus akį į tavo monitorių.

Sėkmės!

### Paleidimas
Migloje kurioje gyvena Karolis kartais atsiranda šviesos spindulių. Vienas jų `docker-compose.yml` šioje repo. 
Aplikacija _turėtu_ pasileisti gana paprastai.
```
docker-compose up
mysql -u root -proot -h 127.0.0.1 < fixture.sql
```

### Kontekstas
Ši aplikaciją - vienas iš daugybės servisų kurie sudaro _amebos_ projektą. Daryk prielaidą, kad vienu metu veiks 100+ šio
serviso instance'ų. Devops'ai pasirūpins serviso build ir paleidimu, bet jie mėgsta bumbėti, tad geriau neduok jiems
peno tą daryti. 

### Reikalavimai
1. Originali serviso specifikacija jau pražuvo, tad negalima laužyti serviso public interface, t.y. response'ai bei url'ai negali kisti.
1. **Visa kita** gali kisti ir vietomis tikimasi rimtų pakeitimų su failų struktūra, naming, schema ir t.t.
1. Iš kodo reikia pašalinti sprendimus kuriuos jau padengia standartinis stack'as
1. Nėra apribojimų bibliotekoms/bundlesams, bet reikia išlaikyti gero kodo [wtf lygį](http://3.bp.blogspot.com/-ilMjE1Gh3Yg/VpUAmd-6TWI/AAAAAAAAAbg/-FJ08zxN42s/s1600/WFTPM.png)
1. Įvykdžius užduotį, reikia suarchyvuoti ir persiųsti visą repozitoriją. Git istorijoje turėtu matytis žingsniai kuriais kodas buvo taisomas.
1. Pridėk `refactoring.md` failą kuriame surašei refactorinimo planą ir kiekviename commit pažymėjai ką padarei. Nes juk nėjai refactorinti _tiesiog_, be jokio plano, _ar ne_?

### Golang variantas
Vavaveeva! Susitarei daryti golang? Tuomet mesk šitą makaroną per langą ir perimplementuok jį su golang

### DUK
#### Ar galiu daryti xyz?
Žiūrėk reikalavimų punktus #1 ir #2

#### Nespėjau padaryti abc!
Jei visą kitą atrodo protingai ir abc detalizuotas `refactoring.md` - užskaitysim kaip dalinai padaryta.
