<?php

declare(strict_types=1);

namespace App\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendMessages extends Command
{
    public static $defaultName = 'mail:send';

    public function __construct(EntityManager $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pdo = $this->entityManager->getConnection();
        $notSent = $pdo->query("select * from mail_queue where `status` != 'sent'")->fetchAll();
        foreach ($notSent as $message) {
            if (strpos($message['types'], 'mail') !== false) {
                mail($message['email'], $message['subject'], $message['mail']);
            }

            if (strpos($message['types'], 'slack') !== false) {
                `curl -X POST -H 'Content-type: application/json' --data '{"icon_emoji":":exclamation:","username":"{$message["subject"]}","channel":"general","mrkdwn":true,"link_names":true,"text":"{$message["mail"]}"}' https://hooks.slack.com/services/xxxxxxxxx/xxxxxxxxx/xxxxxxxxxxxxxxxxxxxxxxxx`;
            }

            $pdo->exec("update mail_queue set `status` =  'sent' where id = " . $message['id']);
        }
        $output->writeln('Ciki-piki!');
    }
}
